﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

//Author By Arman Riazi 2018

namespace Project.DistributedService.WebHostGQ
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>

            new WebHostBuilder()                        
            .UseKestrel(options => {
                            var ip = IPAddress.Parse("::");
                            options.Listen(ip, 8086);                
                            options.AllowSynchronousIO = true;
                            options.Limits.MaxConcurrentConnections = 100;
                            options.Limits.MaxConcurrentUpgradedConnections = 100;
                            options.Limits.MaxRequestHeaderCount = 500;
            })
            .PreferHostingUrls(true)
            .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole(options => options.IncludeScopes = true);
                    logging.AddDebug();
                })
            .UseStartup<Startup>()
            .Build();
    }
}
