﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GQ
{
    public class PluralComponentProjectManagementSystemReport
    {

        public PluralComponentProjectManagementSystemReport() { }

            /// <summary>
            /// <Description></Description>
            /// </summary>          
            public string  NIK_SfodID                           { get; set; }
            public string  NIK_SfoID_fk                         { get; set; }
            public string  NIK_SdtID_fk                         { get; set; }
            public string  NIK_SfodFieldName                    { get; set; }
            public string  NIK_SfodFarsiName                    { get; set; }
            public string  NIk_SfodWidth                        { get; set; }
            public string  NIk_SfodIsHide                       { get; set; }
            public string  NIk_SfodBackColor                    { get; set; }
            public string  NIk_SfodForeColor                    { get; set; }
            public string  NIk_SfodSortState                    { get; set; }
            public string  NIk_SfodGroupState                   { get; set; }
            public string  NIk_SfodFilterState                  { get; set; }
            public string  NIk_SfodAggrigateState               { get; set; }
            public string  NIk_SfodHorizontalMergeCondition     { get; set; }
            public string  NIk_SfodHorizontalMerge              { get; set; }
            public string  NIk_SfodVerticalMergeCondition       { get; set; }
            public string  NIk_SfodVerticalMerge                { get; set; }
            public string  NIk_SfodIsEditable                   { get; set; }
            public string  NIK_SfodCheckValidity                { get; set; }
            public string  NIK_SfodCustomStyle                  { get; set; }
            public string  NIK_SfodNote                         { get; set; }
            public string  NIK_SfodType                         { get; set; }
            public string  NIK_SfodStatus                       { get; set; }
            public string  NIK_SfodRegisterDate                 { get; set; }
            public string  NIK_SfodActive                       { get; set; }
            public string  NIK_SfodDeleteDate                   { get; set; }
            public string  ACC_FinancialYearID                  { get; set; }
            public string TBL_UserID                            { get; set; }
            public string NIK_SfoCustomStyle                    { get; set; }
        

    }


    public class PluralComponentUserProfileHistory
    {

        public PluralComponentUserProfileHistory() { }

        public string TBL_SsID_fk { get; set; }
        public string NIK_SfoID_fk { get; set; }
        public string TBL_UphGridName { get; set; }
        public string TBL_UphBody { get; set; }
        public string TBL_UphNote { get; set; }
        public string TBL_UphRegisterDate { get; set; }
        public string TBL_UphType { get; set; }
        public string TBL_UphActive { get; set; }
        public string TBL_UphStatus { get; set; }
        public string TBL_UphDeleteDate { get; set; }
        public string ACC_FinancialYearID { get; set; }
        public string TBL_UserID { get; set; }
        public string TBL_UphID { get; set; }
    }

    public class PluralComponentAffectUserProfileHistory
    {
        public PluralComponentAffectUserProfileHistory() { }
        /// <summary>
        /// <Description>پیام</Description>
        /// </summary>
        public string AffectedRowCount { get; set; }
    }


}


  
