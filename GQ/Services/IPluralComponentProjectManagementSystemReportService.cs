﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GQ
{


    public interface IPluralComponentProjectManagementSystemReportService
    {

        Task<IEnumerable<PluralComponentProjectManagementSystemReport>> PluralGridInitializeStateProjectManagementSystemGridByParamsAsync(string sfodFieldName,string sfoName);
        Task<IEnumerable<PluralComponentUserProfileHistory>> PluralGridStateUserProfileHistoryByParamsAsync(string subSystemId,string uphGridName, string uphDeleteDate,string userId);
        Task<IEnumerable<PluralComponentAffectUserProfileHistory>> PluralGridAffectStateUserProfileHistoryGridByParamsAsync(
                                                                    string ssID_fk, 
                                                                    string sfoID_fk,
                                                                    string uphGridName,
                                                                    string uphBody,
                                                                    string uphNote,
                                                                    string uphRegisterDate,
                                                                    string uphType,
                                                                    string uphActive,
                                                                    string uphStatus,
                                                                    string uphDeleteDate,
                                                                    string financialYearID,
                                                                    string userID,
                                                                    string uphID);

    }

}