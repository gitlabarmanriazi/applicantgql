﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace GQ
{

    public class PluralComponentProjectManagementSystemReportService : IPluralComponentProjectManagementSystemReportService
    {
        private readonly HttpClient _client;

        public PluralComponentProjectManagementSystemReportService(IHttpClientFactory client)
        {

            _client = client.Create();

        }



        public async Task<IEnumerable<PluralComponentProjectManagementSystemReport>> PluralGridInitializeStateProjectManagementSystemGridByParamsAsync(string sfodFieldName, string sfoName)
        {
            var returnInfo = new List<PluralComponentProjectManagementSystemReport>();

            var stringContent = new StringContent(JsonConvert.SerializeObject(
                new { SfodFieldName = sfodFieldName, SfoName = sfoName }),
                UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage Res = await _client.PostAsync("api/PluralComponentProjectManagementSystemProjectReport/GetInitializeStateProjectManagementSystemGrid", stringContent);

            if (Res.IsSuccessStatusCode)
            {

                var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                returnInfo = JsonConvert.DeserializeObject<List<PluralComponentProjectManagementSystemReport>>(EmpResponse);

            }

            return returnInfo;
        }

        public async Task<IEnumerable<PluralComponentUserProfileHistory>> PluralGridStateUserProfileHistoryByParamsAsync(string subSystemId, string uphGridName, string uphDeleteDate, string userId)
        {
            var returnInfo = new List<PluralComponentUserProfileHistory>();

            var stringContent = new StringContent(JsonConvert.SerializeObject(
                new { SubSystemId= subSystemId, UphGridName = uphGridName, UphDeleteDate = uphDeleteDate ,UserId=userId}),
                UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage Res = await _client.PostAsync("api/PluralComponentProjectManagementSystemProjectReport/GetStateUserProfileHistoryGrid", stringContent);

            if (Res.IsSuccessStatusCode)
            {
                var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                returnInfo = JsonConvert.DeserializeObject<List<PluralComponentUserProfileHistory>>(EmpResponse);
            }

            return returnInfo;
        }

        
        public async Task<IEnumerable<PluralComponentAffectUserProfileHistory>> PluralGridAffectStateUserProfileHistoryGridByParamsAsync(string ssID_fk, string sfoID_fk, string uphGridName, string uphBody, string uphNote, string uphRegisterDate, string uphType, string uphActive, string uphStatus, string uphDeleteDate, string financialYearID, string userID, string uphID)
        {
            var returnInfo = new List<PluralComponentAffectUserProfileHistory>();

            var stringContent = new StringContent(JsonConvert.SerializeObject(
               new
               {
                   SsID_fk=ssID_fk,
                   SfoID_fk = sfoID_fk,
                   UphGridName = uphGridName,
                   UphBody=uphBody,
                   UphNote=uphNote,
                   UphRegisterDate= uphRegisterDate,
                   UphType = uphType,
                   UphActive=uphActive,
                   UphStatus=uphStatus,
                   UphDeleteDate=uphDeleteDate,
                   ACC_FinancialYearID = financialYearID,
                   UserID=userID,
                   UphID=uphID
               }),
                UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage Res = await _client.PostAsync("api/PluralComponentProjectManagementSystemProjectReport/PostStateUserProfileHistoryGrid", stringContent);

            if (Res.IsSuccessStatusCode)
            {

                var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                returnInfo = JsonConvert.DeserializeObject<List<PluralComponentAffectUserProfileHistory>>(EmpResponse);

            }

            return returnInfo;
        }
    }
 }
