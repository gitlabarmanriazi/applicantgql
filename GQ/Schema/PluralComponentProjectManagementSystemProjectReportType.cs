﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace GQ
{

        public class PluralComponentProjectManagementSystemProjectReportType : ObjectGraphType<PluralComponentProjectManagementSystemReport>
        {
            public PluralComponentProjectManagementSystemProjectReportType()
            {
                Field(c => c.ACC_FinancialYearID, nullable: true).Description("ACC_FinancialYearID");
                Field(c => c.NIK_SdtID_fk, nullable: false).Description("NIK_SdtID_fk");
                Field(c => c.NIK_SfodActive, nullable: true).Description("NIK_SfodActive");
                Field(c => c.NIk_SfodAggrigateState, nullable: true).Description("NIk_SfodAggrigateState");
                Field(c => c.NIk_SfodBackColor, nullable: true).Description("NIk_SfodBackColor");
                Field(c => c.NIK_SfodCheckValidity, nullable: true).Description("NIK_SfodCheckValidity");
                Field(c => c.NIK_SfodCustomStyle, nullable: true).Description("NIK_SfodCustomStyle");
                Field(c => c.NIK_SfodFarsiName, nullable: true).Description("NIK_SfodFarsiName");
                Field(c => c.NIK_SfodFieldName, nullable: true).Description("NIK_SfodFieldName");
                Field(c => c.NIk_SfodFilterState, nullable: true).Description("NIk_SfodFilterState");
                Field(c => c.NIk_SfodForeColor, nullable: true).Description("NIk_SfodForeColor");
                Field(c => c.NIk_SfodGroupState, nullable: true).Description("NIk_SfodGroupState");
                Field(c => c.NIk_SfodHorizontalMerge, nullable: true).Description("NIk_SfodHorizontalMerge");
                Field(c => c.NIk_SfodHorizontalMergeCondition, nullable: true).Description("NIk_SfodHorizontalMergeCondition");
                Field(c => c.NIK_SfodID, nullable: true).Description("NIK_SfodID");
                Field(c => c.NIk_SfodIsEditable, nullable: true).Description("NIk_SfodIsEditable");
                Field(c => c.NIk_SfodIsHide, nullable: true).Description("NIk_SfodIsHide");
                Field(c => c.NIK_SfodNote, nullable: true).Description("NIK_SfodNote");
                Field(c => c.NIK_SfodRegisterDate, nullable: true).Description("NIK_SfodRegisterDate");
                Field(c => c.NIk_SfodSortState, nullable: true).Description("NIk_SfodSortState");
                Field(c => c.NIK_SfodType, nullable: true).Description("NIK_SfodType");
                Field(c => c.NIk_SfodVerticalMerge, nullable: true).Description("NIk_SfodVerticalMerge");
                Field(c => c.NIk_SfodVerticalMergeCondition, nullable: true).Description("NIk_SfodVerticalMergeCondition");
                Field(c => c.NIk_SfodWidth, nullable: true).Description("NIk_SfodWidth");
                Field(c => c.NIK_SfoID_fk, nullable: true).Description("NIK_SfoID_fk");
                Field(c => c.TBL_UserID, nullable: true).Description("TBL_UserID");
                Field(c => c.NIK_SfoCustomStyle, nullable: true).Description("NIK_SfoCustomStyle");

        }
    }
        public class PluralComponentUserProfileHistoryType : ObjectGraphType<PluralComponentUserProfileHistory>
        {
            public PluralComponentUserProfileHistoryType()
            {
                Field(c => c.ACC_FinancialYearID, nullable: true).Description("ACC_FinancialYearID");
                Field(c => c.TBL_SsID_fk, nullable: false).Description("TBL_SsID_fk");
                Field(c => c.NIK_SfoID_fk, nullable: true).Description("NIK_SfoID_fk");
                Field(c => c.TBL_UphActive, nullable: true).Description("TBL_UphActive");
                Field(c => c.TBL_UphBody, nullable: true).Description("TBL_UphBody");
                Field(c => c.TBL_UphDeleteDate, nullable: true).Description("TBL_UphDeleteDate");
                Field(c => c.TBL_UphGridName, nullable: true).Description("TBL_UphGridName");
                Field(c => c.TBL_UphID, nullable: true).Description("TBL_UphID");
                Field(c => c.TBL_UphNote, nullable: true).Description("TBL_UphNote");
                Field(c => c.TBL_UphRegisterDate, nullable: true).Description("TBL_UphRegisterDate");
                Field(c => c.TBL_UphStatus, nullable: true).Description("TBL_UphStatus");
                Field(c => c.TBL_UphType, nullable: true).Description("TBL_UphType");            
                Field(c => c.TBL_UserID, nullable: true).Description("TBL_UserID");
            }
        }
        public class PluralComponentAffectUserProfileHistoryType : ObjectGraphType<PluralComponentAffectUserProfileHistory>
        {
            public PluralComponentAffectUserProfileHistoryType()
            {
                Field(c => c.AffectedRowCount, nullable: true).Description("AffectedRowCount");
            }
        }
}

